﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class waveCycle : MonoBehaviour
{
    //Current Wave
    public static int currentWave;

    //Day time when true, night time when false
    public static bool dayTime;

    //Modifiable variable to set day/night cycle lengths in minutes
    public int dayCycleMin = 3;
    public int nightCycleMin = 3;

    //Stores cycle lengths in seconds
    private int dayCycleSec;
    private int nightCycleSec;

    //Seconds until day/night cycle change
    public static int currentCycleSec;

    //stores formatted min/sec for UI clock
    private int clockMin;
    private int clockSec;

    //enemy spawner
    public GameObject spawner;

    //UI GameObjects
    public GameObject clockDayNightUI;
    public GameObject waveCounterUI;
    public GameObject nightFilter;

    //UI text components from GameObjectsS
    private TMP_Text clockDayNightText;
    private TMP_Text waveCounterText;

    //variable to track runtime. used to track elapsed time in seconds
    private int runtime;

    //fade to day/night animator
    public Animator dayNightFade;

    // Start is called before the first frame update
    void Start()
    {
        currentWave = 1;
        dayTime = true;

        //calculate day/night cycle lengths in seconds
        dayCycleSec = dayCycleMin * 60;
        nightCycleSec = nightCycleMin * 60;


        //--USED FOR RAPID TESTING--
        //dayCycleSec = 20;
        //nightCycleSec = 15;


        //set current cycle length to day seconds since we start on day
        currentCycleSec = dayCycleSec;

        //get text components from UI GameObjects
        clockDayNightText = clockDayNightUI.GetComponent<TMP_Text>();
        waveCounterText = waveCounterUI.GetComponent<TMP_Text>();

        //get current runtime. used in update to decrement currentCycleSec each second.
        runtime = (int) Time.fixedTime;

        //update UI
        updateDayNightClock();
        updateWaveCounter();
    }

    // Update is called once per frame
    void Update()
    {
        //when cycle timer runs down, flip day/night cycle
        if (currentCycleSec == 0)
        {
            if (dayTime)
            {
                setNightTime();
            }
            else
            {
                setDayTime();
            }
            updateMoveBlocking();
        }

        //Perform day/night fade animation
        if(currentCycleSec == 5)
        {
            if(dayTime)
            {
                dayNightFade.ResetTrigger("dayFade");
                dayNightFade.SetTrigger("nightFade");
            }
            else
            {
                dayNightFade.ResetTrigger("nightFade");
                dayNightFade.SetTrigger("dayFade");
            }
        }

        //Time.fixedTime is current runtime in sec. update clock if second has elapsed
        if (runtime != (int) Time.fixedTime)
        {
            currentCycleSec--;
            updateDayNightClock();
            runtime = (int) Time.fixedTime;
        }
        
    }

    private void setNightTime()
    {
        //set night & enable night filter
        dayTime = false;

        //reset countdown to nightCycleSec
        currentCycleSec = nightCycleSec;
    }

    private void setDayTime()
    {
        //set day & disable night filter
        dayTime = true;

        //reset countdown to dayCycleSec
        currentCycleSec = dayCycleSec;

        //increment wave & update UI
        currentWave++;
        updateWaveCounter();

        killAllEnemies();
        resetStructureHealths();

        spawner.GetComponent<enemySpawner>().nextWave();
    }

    private void updateMoveBlocking()
    {
        GameObject[] structures = GameObject.FindGameObjectsWithTag("Structure");

        foreach (GameObject s in structures)
        {
            s.transform.Find("StructureSelect").transform.Find("MoveButton").gameObject.GetComponent<Button>().interactable = dayTime;
        }
    }

    private void getClockValues()
    {
        //get minutes from floor division
        clockMin = currentCycleSec / 60;

        //get seconds from modulo (remainder)
        clockSec = currentCycleSec % 60;
    }

    private void updateDayNightClock()
    {
        string clockSprite;

        //choose proper day/night sprite
        if(dayTime)
        {
            clockSprite = "<sprite=0>";
        }
        else
        {
            clockSprite = "<sprite=1>";
        }

        getClockValues();

        //format text string for clock UI
        clockDayNightText.text = clockSprite + clockMin.ToString("D2") + ":" + clockSec.ToString("D2");
    }

    private void updateWaveCounter()
    {
        waveCounterText.text = "Wave " + currentWave; 
    }

    private void resetStructureHealths()
    {
        GameObject[] structures = GameObject.FindGameObjectsWithTag("Structure");

        foreach (GameObject structure in structures)
        {
            structure.GetComponent<healthBar>().resetHealth();
        }
    }
    private void killAllEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemy in enemies)
        {
            enemy.GetComponent<healthBar>().setHealth(0);
        }
    }
}
