﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawner : MonoBehaviour
{
    [Header("Enemy GameObjects")]
    public GameObject skeleton;
    public GameObject goblin;

    [Header("Enemy Scriptable Objects")]
    public Enemy Skeleton;
    public Enemy Goblin;

    private GameObject[] enemies = new GameObject[2];

    [Header("Enemy Spawn Settings")]
    public int enemiesPerInterval;
    public int spawnInterval;
    public int stopSpawnTime;

    [Header("Incremental Wave Settings")]
    public int extraEnemiesPerInterval;
    //public int extraEnemyHealthPerWave;
    //public int extraEnemyDamagePerWave;

    private int enemiesToSpawn;


    //variable to track runtime. used to track elapsed time in seconds
    private int runtime;

    // Start is called before the first frame update
    void Start()
    {
        enemies[0] = skeleton;
        enemies[1] = goblin;

        enemiesToSpawn = enemiesPerInterval;
    }

    // Update is called once per frame
    void Update()
    {
        //spawn enemies during night if (spawn interval) seconds has elapsed and more than (stopSpawnTime) seconds remain in night
        if (!waveCycle.dayTime && (runtime + spawnInterval <= (int)Time.fixedTime) && waveCycle.currentCycleSec > stopSpawnTime)
        {
            spawnEnemies();
            runtime = (int)Time.fixedTime;
        }
    }

    void spawnEnemies()
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            //get random enemy from enemies[] array and instantiate it
            GameObject randomEnemy = enemies[Mathf.FloorToInt(Random.Range(0, enemies.Length))];
            GameObject spawnedEnemy = Instantiate(randomEnemy, getSpawnLocation(), randomEnemy.transform.rotation);

            //set health
            Enemy enemy = spawnedEnemy.GetComponent<enemyAI>().enemy;
            spawnedEnemy.GetComponent<healthBar>().setMaxHealth(enemy.health);
        }
    }

    Vector2 getSpawnLocation()
    {
        Vector2 spawnPos;
        float posX, posY, spawnSide;

        spawnSide = Mathf.FloorToInt(Random.Range(0, 4));

        switch(spawnSide)
        {
            //Left side
            case 0:
                posX = -15.5f;
                posY = Mathf.Floor(Random.Range(-10, 11) + 0.5f);
                break;

            //Right side
            case 1:
                posX = 15.5f;
                posY = Mathf.Floor(Random.Range(-10, 11) + 0.5f);
                break;

            //Top side
            case 2:
                posY = 10.5f;
                posX = Mathf.Floor(Random.Range(-15, 16) + 0.5f);
                break;

            //Bottom side
            case 3:
                posY = -10.5f;
                posX = Mathf.Floor(Random.Range(-15, 16) + 0.5f);
                break;

            //error
            default:
                posX = 0;
                posY = 0;
                Debug.LogError("Nonexistent Spawn Side");
                break;
        }

        spawnPos = new Vector2(posX, posY);
        return spawnPos;
    }

    public void nextWave()
    {
        //increase enemies
        enemiesToSpawn += extraEnemiesPerInterval;

        //increase enemy health
        /*Skeleton.health += extraEnemyHealthPerWave;
        Goblin.health += extraEnemyHealthPerWave;

        //increase enemy damage
        Skeleton.damage += extraEnemyHealthPerWave;
        Goblin.damage += extraEnemyHealthPerWave;*/
    }
}
