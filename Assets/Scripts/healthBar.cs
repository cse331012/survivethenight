﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBar : MonoBehaviour
{
    public Slider healthSlider;
    public int health;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(health != healthSlider.maxValue)
        {
            gameObject.transform.Find("Healthbar").gameObject.SetActive(true);
        }
        else
        {
            gameObject.transform.Find("Healthbar").gameObject.SetActive(false);
        }
        checkForDestroy();
    }

    public void takeDamage(int damage)
    {
        if (gameObject.TryGetComponent<enemyAI>(out enemyAI enemyai))
        {
            enemyai.takeDamageAnimation();
        }

        setHealth(health-damage);
    }

    public void resetHealth()
    {
        setHealth((int) healthSlider.maxValue);
    }

    public void setMaxHealth(int h)
    {
        healthSlider.maxValue = h;
        setHealth(h);
    }

    public void setHealth(int h)
    {
        health = h;
        updateHealthBar();
    }

    public void updateHealthBar()
    {
        healthSlider.value = health;
    }
    public void checkForDestroy()
    {
        if(health <= 0)
        {
            if(gameObject.TryGetComponent<enemyAI>(out enemyAI enemyai))
            {
                StartCoroutine(enemyai.die());
                return;
            }

            Destroy(gameObject);

            //recalculate AI pathfinding graph
             AstarPath.active.Scan();
        }
    }
}
