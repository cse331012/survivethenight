﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class buildMenu : MonoBehaviour
{
    //build menu UI
    public GameObject buildMenuUI;

    //maximum structure count
    /*public int MaxHarvesters;
    public int MaxDefenses;*/

    //each structure button
    public GameObject fenceButton;
    public GameObject lumberjackButton;
    public GameObject stoneMineButton;
    public GameObject archerButton;
    public GameObject cannonButton;

    [Header("Level 1 Structures")]
    public Fence fence;
    public Lumberjack lumberjack;
    public StoneMine stoneMine;
    public Archer archer;
    public Cannon cannon;

    private void Start()
    {
        //get button text components
        TMP_Text fenceButtonWoodText = fenceButton.transform.Find("WoodCost TMP").gameObject.GetComponent<TMP_Text>();
        TMP_Text fenceButtonStoneText = fenceButton.transform.Find("StoneCost TMP").gameObject.GetComponent<TMP_Text>();

        TMP_Text lumberjackButtonWoodText = lumberjackButton.transform.Find("WoodCost TMP").gameObject.GetComponent<TMP_Text>();
        TMP_Text lumberjackButtonStoneText = lumberjackButton.transform.Find("StoneCost TMP").gameObject.GetComponent<TMP_Text>();

        TMP_Text stoneMineButtonWoodText = stoneMineButton.transform.Find("WoodCost TMP").gameObject.GetComponent<TMP_Text>();
        TMP_Text stoneMineButtonStoneText = stoneMineButton.transform.Find("StoneCost TMP").gameObject.GetComponent<TMP_Text>();

        TMP_Text archerButtonWoodText = archerButton.transform.Find("WoodCost TMP").gameObject.GetComponent<TMP_Text>();
        TMP_Text archerButtonStoneText = archerButton.transform.Find("StoneCost TMP").gameObject.GetComponent<TMP_Text>();

        TMP_Text cannonButtonWoodText = cannonButton.transform.Find("WoodCost TMP").gameObject.GetComponent<TMP_Text>();
        TMP_Text cannonButtonStoneText = cannonButton.transform.Find("StoneCost TMP").gameObject.GetComponent<TMP_Text>();

        //insert costs into button texts
        /*fenceButtonWoodText.text = "<sprite=0> " + fence.buildCostWood;
        fenceButtonStoneText.text = "<sprite=0> " + fence.buildCostStone;*/
        fenceButtonWoodText.text = "<sprite=0> -";
        fenceButtonStoneText.text = "<sprite=0> -";

        lumberjackButtonWoodText.text = "<sprite=0> " + lumberjack.buildCostWood;
        lumberjackButtonStoneText.text = "<sprite=0> " + lumberjack.buildCostStone;

        stoneMineButtonWoodText.text = "<sprite=0> " + stoneMine.buildCostWood;
        stoneMineButtonStoneText.text = "<sprite=0> " + stoneMine.buildCostStone;

        archerButtonWoodText.text = "<sprite=0> " + archer.buildCostWood;
        archerButtonStoneText.text = "<sprite=0> " + archer.buildCostStone;

        cannonButtonWoodText.text = "<sprite=0> " + cannon.buildCostWood;
        cannonButtonStoneText.text = "<sprite=0> " + cannon.buildCostStone;

    }

    private void Update()
    {
        //updates button states to only be used if ample resources are present
        updateButtons();
    }

    private void updateButtons()
    {
        if (rCountWood.woodCount < fence.buildCostWood || rCountStone.stoneCount < fence.buildCostStone)
        {
            fenceButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            //fenceButton.GetComponent<Button>().interactable = true;
        }

        if (rCountWood.woodCount < lumberjack.buildCostWood || rCountStone.stoneCount < lumberjack.buildCostStone)
        {
            lumberjackButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            lumberjackButton.GetComponent<Button>().interactable = true;
        }

        if (rCountWood.woodCount < stoneMine.buildCostWood || rCountStone.stoneCount < stoneMine.buildCostStone)
        {
            stoneMineButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            stoneMineButton.GetComponent<Button>().interactable = true;
        }

        if (rCountWood.woodCount < archer.buildCostWood || rCountStone.stoneCount < archer.buildCostStone)
        {
            archerButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            archerButton.GetComponent<Button>().interactable = true;
        }

        if (rCountWood.woodCount < cannon.buildCostWood || rCountStone.stoneCount < cannon.buildCostStone)
        {
            cannonButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            cannonButton.GetComponent<Button>().interactable = true;
        }
    }

    public void OnBuildButtonPress()
    {
        if(buildMenuUI.activeSelf)
        {
            exitBuildMenu();
        }
        else
        {
            openBuildMenu();
        }
    }

    private void openBuildMenu()
    {
        buildMenuUI.SetActive(true);

        structureFunctions.blockSelect = true;
        treeHarvest.blockHarvest = true;
    }

    public void exitBuildMenu()
    {
        buildMenuUI.SetActive(false);

        structureFunctions.blockSelect = false;
        treeHarvest.blockHarvest = false;
    }

    public void buildStructure(GameObject structure)
    {
        exitBuildMenu();

        GameObject newStructure = Instantiate(structure, new Vector2(0, 0), structure.transform.rotation);
        newStructure.name = newStructure.name.Replace("(Clone)", "");

        newStructure.GetComponent<structureFunctions>().initialPlacing();
    }
}
