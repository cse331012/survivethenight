﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class spawnTrees : MonoBehaviour
{
    public int numTrees;
    public GameObject tree;
    public GameObject boundary;

    void Start()
    {
        spawnObjects();
    }

    public void spawnObjects()
    {
        //Get spawn boundaries
        MeshCollider b = boundary.GetComponent<MeshCollider>();

        float screenX, screenY;
        Vector2 pos;

        //Spawn numTrees trees in spawn boundaries
        for (int i = 0; i < numTrees; i++)
        {
            //newPosFlag used to determine if new position or if tree already present
            bool newPosFlag;

            //find all existing trees
            GameObject[] trees = GameObject.FindGameObjectsWithTag("Tree");

            //get random position for new tree, if tre already exists here loop again
            do
            {
                newPosFlag = true;

                //get random x & y and position in grid
                screenX = Mathf.Floor(Random.Range(b.bounds.min.x, b.bounds.max.x)) + 0.5f;
                screenY = Mathf.Floor(Random.Range(b.bounds.min.y, b.bounds.max.y)) + 0.5f;
                pos = new Vector2(screenX, screenY);

                //generate new coordinates if tree spawns over HomeBase
                if(screenX < 1 && screenX > -1 && screenY < 1 && screenY > -1)
                {
                    newPosFlag = false;
                    //Debug.Log("Tree generated on HomeBase.");
                    continue;
                }

                //check if trees spawn over UI elements
                if ((screenY == 8.5f && (screenX <= -9 || screenX >= 5)) || (screenY == -8.5f && (screenX <= -10 || screenX >= 11)))
                {
                    newPosFlag = false;
                    //Debug.Log("Tree generated in UI.");
                    continue;
                }

                //check if tree already exists in this location
                foreach (GameObject t in trees)
                {
                    Vector2 tPos = new Vector2(t.transform.position.x, t.transform.position.y);
                    if (tPos == pos)
                    {
                        //Debug.Log("Previous tree location detected.");
                        newPosFlag = false;
                        break;
                    }
                }

            } while (!newPosFlag);

            //Instantiate new tree
            Instantiate(tree, pos, tree.transform.rotation);
        }

        //recalculate AI pathfinding graph
        AstarPath.active.Scan();
    }
}
