﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class enemyAI : MonoBehaviour
{
    private enum State
    {
        idle,
        moving,
        attack
    }
    private State state;

    public Enemy enemy;
    public Animator animator;

    private AIDestinationSetter aiDest;
    private AIPath aiPath;

    private GameObject attackTarget;

    //variable to track runtime. used to track elapsed time in seconds
    private int runtime;

    // Start is called before the first frame update
    void Start()
    {
        aiDest = gameObject.GetComponent<AIDestinationSetter>();
        aiPath = gameObject.GetComponent<AIPath>();

        setIdle();

        //set closest target for AI
        setClosestTarget();
    }

    // Update is called once per frame
    void Update()
    {
        switch(state)
        {
            case State.moving:
                //if stopped moving, end of path reached. Begin attacking target structure.
                if (aiPath.desiredVelocity.x == 0f && aiPath.desiredVelocity.y == 0f)
                {
                    setAttack();
                }
                break;
            case State.attack:
                //if the attackTarget exists, attack it every (enemy.attackTime) seconds
                if(attackTarget)
                {
                    if (runtime + enemy.attackTime <= (int)Time.fixedTime)
                    {
                        enemy.attack(attackTarget);
                        runtime = (int)Time.fixedTime;
                    }
                }
                //if attackTarget does not exist (destroyed), find a new target, idle while waiting for coroutine
                else
                {
                    setClosestTarget();
                    setIdle();
                }
                break;
            default:
                break;
        }
        //flip x orientation of enemy sprite to match the x velocity component 
        updateOrientation();
    }

    public void takeDamageAnimation()
    {
        animator.ResetTrigger("TakeHit");
        animator.SetTrigger("TakeHit");
    }

    public IEnumerator die()
    {
        animator.SetBool("Dead", true);
        aiPath.canMove = false;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    public void setIdle()
    {
        state = State.idle;
        aiPath.canMove = false;
        animator.SetBool("Move", false);
        animator.SetBool("Attack", false);
    }

    private void setMoving()
    {
        state = State.moving;
        aiPath.canMove = true;
        animator.SetBool("Move", true);
        animator.SetBool("Attack", false);
    }

    private void setAttack()
    {
        state = State.attack;
        animator.SetBool("Attack", true);
        //animator.SetBool("Move", false);
    }

    private void setClosestTarget()
    {
        StartCoroutine(getClosestTarget());
    }

    IEnumerator getClosestTarget()
    {
        //track shortest Path. set this high and update when shorter pathLength found
        float shortestPath = 1000f, pathLength;

        //store closest structure when new shortestPath is found. temporarily store gameObject transform
        GameObject closestStructure = gameObject;

        //get AI seeker
        Seeker seeker = gameObject.GetComponent<Seeker>();

        //Find all Structures
        GameObject[] structures = GameObject.FindGameObjectsWithTag("Structure");
        //Debug.Log("found " + structures.Length + " structures");

        foreach (GameObject structure in structures)
        {
            //if structure has been destroyed then move on to next
            if (!structure) continue;

            Path path = seeker.StartPath(gameObject.transform.position, structure.transform.position);

            //wait for seeker to finish path calculation
            yield return StartCoroutine(path.WaitForPath());

            pathLength = path.GetTotalLength();

            //Debug.Log("Path length to " + s.name + ": " + pathLength);

            //update shortestPath and closestStructure if shorter path found
            if(pathLength < shortestPath)
            {
                shortestPath = pathLength;
                closestStructure = structure;
            }
        }

        if(closestStructure)
        {
            //set destination & attack target to closest structure
            aiDest.target = closestStructure.transform;
            attackTarget = closestStructure;  
        }

        setMoving();
    }

    private void updateOrientation()
    {
        if (aiPath.desiredVelocity.x <= 0.01f)
        {
            transform.localScale = new Vector3(-1.5f, 1.5f, 1f);
            transform.Find("Healthbar").localScale = new Vector3(-0.0196f, 0.0196f, 1f);
        }
        else if (aiPath.desiredVelocity.x >= -0.01f)
        {
            transform.localScale = new Vector3(1.5f, 1.5f, 1f);
            transform.Find("Healthbar").localScale = new Vector3(0.0196f, 0.0196f, 1f);
        }
    }
}
