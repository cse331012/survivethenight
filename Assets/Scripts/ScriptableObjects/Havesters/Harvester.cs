﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Harvester : Structure
{
    public int buildCostWood;
    public int buildCostStone;

    public abstract void harvest();

    public override void upgradeStructure(GameObject structure)
    {
        structure.GetComponent<harvestResource>().updateHarvester();
    }

    public override void setRadiusUI(GameObject structure) {
        return;
    }

    public override void takeBuildCost()
    {
        rCountWood.woodCount -= buildCostWood;
        rCountStone.stoneCount -= buildCostStone;
    }

    public override int getStat2Val()
    {
        return -1;
    }
}
