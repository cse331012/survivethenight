﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "StoneMine", menuName = "Structures/Harvester/StoneMine")]
public class StoneMine : Harvester
{
    public int stoneHarvestRate;

    public override void harvest()
    {
        rCountStone.stoneCount += stoneHarvestRate;
    }

    public override void updateUniqueUpgradeMenuStats(GameObject upgradeMenu)
    {
        upgradeMenu.transform.Find("Stat1").gameObject.GetComponent<TMP_Text>().text = "Harvest:\t" + stoneHarvestRate;
    }

    public override int getStat1Val()
    {
        return stoneHarvestRate;
    }
}
