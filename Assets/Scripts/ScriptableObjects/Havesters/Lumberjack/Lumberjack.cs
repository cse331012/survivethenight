﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Lumberjack", menuName = "Structures/Harvester/Lumberjack")]
public class Lumberjack : Harvester
{
    public int woodHarvestRate;

    public override void harvest()
    {
        rCountWood.woodCount += woodHarvestRate;
    }

    public override void updateUniqueUpgradeMenuStats(GameObject upgradeMenu)
    {
        upgradeMenu.transform.Find("Stat1").gameObject.GetComponent<TMP_Text>().text = "Harvest:\t" + woodHarvestRate;
    }
    public override int getStat1Val()
    {
        return woodHarvestRate;
    }
}
