﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Structure : ScriptableObject
{
    public int health;
    public int upgradeCostWood;
    public int upgradeCostStone;

    public abstract void takeBuildCost();
    public abstract void upgradeStructure(GameObject structure);
    public abstract void setRadiusUI(GameObject structure);

    public void takeUpgradeCost()
    {
        rCountWood.woodCount -= upgradeCostWood;
        rCountStone.stoneCount -= upgradeCostStone;
    }

    public abstract void updateUniqueUpgradeMenuStats(GameObject upgradeMenu);
    public abstract int getStat1Val();
    public abstract int getStat2Val();

}
