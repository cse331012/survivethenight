﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Fence", menuName = "Structures/Fence")]
public class Fence : Structure
{
    public int buildCostWood;
    public int buildCostStone;

    public override void upgradeStructure(GameObject structure)
    {
        rCountWood.woodCount -= buildCostWood;
        rCountStone.stoneCount -= buildCostStone;
    }

    public override void setRadiusUI(GameObject structure)
    {
        return;
    }

    public override void takeBuildCost()
    {
        return;
    }

    public override int getStat1Val()
    {
        return -1;
    }

    public override int getStat2Val()
    {
        return -1;
    }

    public override void updateUniqueUpgradeMenuStats(GameObject upgradeMenu)
    {
        return;
    }
}
