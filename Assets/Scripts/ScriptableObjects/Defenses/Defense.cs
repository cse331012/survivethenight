﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class Defense : Structure
{
    public int buildCostWood;
    public int buildCostStone;

    public int damage;
    public int radius;
    public float fireTime;
    public override void upgradeStructure(GameObject structure)
    {
        setRadiusUI(structure);
        structure.GetComponent<defenseAI>().defense = (Defense) structure.GetComponent<upgradeHandler>().structure;
    }

    public override void setRadiusUI(GameObject structure)
    {
        structure.transform.Find("StructureSelect").Find("RangeCircle").localScale = new Vector3(radius + 1f, radius + 1f, 0f);
    }

    public override void updateUniqueUpgradeMenuStats(GameObject upgradeMenu)
    {
        upgradeMenu.transform.Find("Stat1").gameObject.GetComponent<TMP_Text>().text = "Damage:\t" + damage;
        upgradeMenu.transform.Find("Stat2").gameObject.GetComponent<TMP_Text>().text = "Radius:\t" + radius;
    }

    public override int getStat1Val()
    {
        return damage;
    }

    public override int getStat2Val()
    {
        return radius;
    }

    public override void takeBuildCost()
    {
        rCountWood.woodCount -= buildCostWood;
        rCountStone.stoneCount -= buildCostStone;
    }
}
    
