﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HomeBase", menuName = "Structures/HomeBase")]
public class HomeBase : Structure
{
    public override void upgradeStructure(GameObject structure)
    {
        return;
    }

    public override void setRadiusUI(GameObject structure)
    {
        return;
    }

    public override void takeBuildCost()
    {
        return;
    }

    public override int getStat1Val()
    {
        return -1;
    }

    public override int getStat2Val()
    {
        return -1;
    }

    public override void updateUniqueUpgradeMenuStats(GameObject upgradeMenu)
    {
        return;
    }
}
