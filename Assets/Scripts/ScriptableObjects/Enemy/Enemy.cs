﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy")]
public class Enemy : ScriptableObject
{
    public int health;
    public int damage;
    public int attackTime;

    public void attack(GameObject target)
    {
        target.GetComponent<healthBar>().takeDamage(damage);
    }
}
