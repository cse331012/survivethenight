﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class monitorHomeBase : MonoBehaviour
{

    public GameObject homeBase;
    public GameObject endGameUI;

    private bool homeBaseDestroyed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!homeBase && !homeBaseDestroyed)
        {
            Time.timeScale = 0f;
            Debug.Log("Home Base Destroyed!");
            haltEnemies();
            homeBaseDestroyed = true;

            //update and enable endGameUI menu
            int wavesSurvived = waveCycle.currentWave - 1;
            if (wavesSurvived == 1)
                endGameUI.transform.Find("WavesSurvivedText").GetComponent<TMP_Text>().text = "You Survived " + wavesSurvived + " Wave";
            else
                endGameUI.transform.Find("WavesSurvivedText").GetComponent<TMP_Text>().text = "You Survived " + wavesSurvived + " Waves";

            endGameUI.SetActive(true);
        }
    }

    private void haltEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject enemy in enemies)
        {
            enemy.GetComponent<enemyAI>().setIdle();
        }
    }
}
