﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class treeHarvest : MonoBehaviour
{
    //blockHarvest stops trees from being harvested - used when menus are open
    public static bool blockHarvest = false;

    //When tree object clicked, remove it from scene and collect 10 wood
    private void OnMouseDown()
    {
        if (blockHarvest) return;
        Destroy(gameObject);
        rCountWood.woodCount+=10;

        //recalculate AI pathfinding graph
        AstarPath.active.Scan();
    }
}
