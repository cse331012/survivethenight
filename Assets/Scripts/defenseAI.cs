﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class defenseAI : MonoBehaviour
{

    public GameObject projectile;

    [HideInInspector]
    public Defense defense;

    private int runtime;

    // Start is called before the first frame update
    void Start()
    {
        defense = (Defense) gameObject.GetComponent<upgradeHandler>().structure;

    }

    // Update is called once per frame
    void Update()
    {
        if (runtime + defense.fireTime <= (int)Time.fixedTime)
        {
            GameObject target = getClosestEnemy();
            if (target)
            {
                projectileHandler.createProjectile(projectile, gameObject.transform.Find("FirePosition").position, target, defense.damage);
            }
            runtime = (int)Time.fixedTime;
        }
    }

    public GameObject getClosestEnemy()
    {
        GameObject closestEnemy = null;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemy in enemies)
        {
            //if enemy has died do not consider it
            if (!enemy) continue;

            //only check enemies within fire radius of defense
            if (Vector3.Distance(gameObject.transform.position, enemy.transform.position) <= defense.radius+1)
            {
                //if we havent found an enemy already, choose this one
                if (!closestEnemy)
                {
                    closestEnemy = enemy;
                }
                else
                {
                    //otherwise check enemy distance against closestEnemy distance
                    if (Vector3.Distance(gameObject.transform.position, enemy.transform.position) <
                       Vector3.Distance(gameObject.transform.position, closestEnemy.transform.position))
                    {
                        closestEnemy = enemy;
                    }
                }
            }
        }
        return closestEnemy;
    }
}
