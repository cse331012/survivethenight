﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class upgradeHandler : MonoBehaviour
{    
    //Upgrade Menu UI Game Object
    public GameObject upgradeMenuUI;

    //current structure level - start at 1
    [HideInInspector]
    public int level = 1;

    //Structure level scriptable objects
    public Structure level1Structure;
    public Structure level2Structure;
    public Structure level3Structure;

    [HideInInspector]
    public Structure[] structureLevels = new Structure[4];

    //Current structure level scriptable object
    [HideInInspector]
    public Structure structure;

    // Start is called before the first frame update
    void Start()
    {
        //populate structureLevels array
        structureLevels[0] = null;
        structureLevels[1] = level1Structure;
        structureLevels[2] = level2Structure;
        structureLevels[3] = level3Structure;

        //set to level 1 structure
        updateStructureLevel();

        //populate upgrade menu fields
        updateUpgradeMenu();
    }

    private void Update()
    {
        //updates button states to only be used if ample resources are present

        if(!fullyUpgraded())
        {
            updateUpgradeButton();
        }
    }

    public void upgradeStructureLevel()
    {
        level++;
        structure.takeUpgradeCost();
        updateStructureLevel();
        updateUpgradeMenu();
        structure.upgradeStructure(gameObject);

        updateStructureSpriteStars();
    }

    private void updateStructureLevel()
    {
        structure = structureLevels[level];

        gameObject.GetComponent<healthBar>().setMaxHealth(structure.health);
    }

    public void OnUpgradeMenuButtonPress()
    {
        gameObject.GetComponent<structureFunctions>().deselectStructure();
        openUpgradeMenu();
    }

    public void OnUpgradeButtonPress()
    {
        upgradeStructureLevel();
        closeUpgradeMenu();
    }

    private void updateUpgradeMenu()
    {
        if (fullyUpgraded())
        {
            upgradeMenuUI.transform.Find("Cost").gameObject.SetActive(false);
            upgradeMenuUI.transform.Find("UpgradeStatHealth").gameObject.GetComponent<TMP_Text>().text = "-";
            upgradeMenuUI.transform.Find("UpgradeStat1").gameObject.GetComponent<TMP_Text>().text = "-";
            upgradeMenuUI.transform.Find("UpgradeStat2").gameObject.GetComponent<TMP_Text>().text = "-";
            updateUpgradeButton();
        }
        else
        {
            upgradeMenuUI.transform.Find("UpgradeStatHealth").gameObject.GetComponent<TMP_Text>().text = structureLevels[level+1].health.ToString();
            upgradeMenuUI.transform.Find("UpgradeStat1").gameObject.GetComponent<TMP_Text>().text = structureLevels[level + 1].getStat1Val().ToString();
            upgradeMenuUI.transform.Find("UpgradeStat2").gameObject.GetComponent<TMP_Text>().text = structureLevels[level + 1].getStat2Val().ToString();
        }
        //update common stats
        upgradeMenuUI.transform.Find("Name").gameObject.GetComponent<TMP_Text>().text = gameObject.name;
        upgradeMenuUI.transform.Find("Level").gameObject.GetComponent<TMP_Text>().text = "Level " + level;
        upgradeMenuUI.transform.Find("StatHealth").gameObject.GetComponent<TMP_Text>().text = "Health:\t" + structure.health;
        upgradeMenuUI.transform.Find("Cost").gameObject.GetComponent<TMP_Text>().text = "Cost: <sprite=0>" + structure.upgradeCostWood + " <sprite=1>" + structure.upgradeCostStone;

        //update unique stats
        structure.updateUniqueUpgradeMenuStats(upgradeMenuUI);
    }

    private void updateUpgradeButton()
    {
        if(fullyUpgraded())
        {
            upgradeMenuUI.transform.Find("UpgradeButton").gameObject.GetComponentInChildren<TMP_Text>().text = "Fully Upgraded!";
            upgradeMenuUI.transform.Find("UpgradeButton").gameObject.GetComponent<Button>().interactable = false;
            return;
        }

        if (rCountWood.woodCount < structure.upgradeCostWood || rCountStone.stoneCount < structure.upgradeCostStone)
        {
            upgradeMenuUI.transform.Find("UpgradeButton").gameObject.GetComponent<Button>().interactable = false;
        }
        else
        {
            upgradeMenuUI.transform.Find("UpgradeButton").gameObject.GetComponent<Button>().interactable = true;
        }
    }

    private void openUpgradeMenu()
    {
        centerUpgradeMenu();
        upgradeMenuUI.SetActive(true);

        structureFunctions.blockSelect = true;
        treeHarvest.blockHarvest = true;
    }

    public void closeUpgradeMenu()
    {
        upgradeMenuUI.SetActive(false);

        structureFunctions.blockSelect = false;
        treeHarvest.blockHarvest = false;
    }

    //hacky fix to replace menu in center after moving object
    private void centerUpgradeMenu()
    {
        Vector2 repos = new Vector2();
        repos.x = -(gameObject.transform.position.x/2);
        repos.y = -(gameObject.transform.position.y/2);
        upgradeMenuUI.transform.localPosition = repos;
    }

    public bool fullyUpgraded()
    {
        if (structure is Fence) return (level == structureLevels.Length - 2);
        return (level == structureLevels.Length - 1);
    }

    private void updateStructureSpriteStars()
    {
        if (structure is Fence) return;

        if (level == 2)
        {
            gameObject.transform.Find("Level2Star").gameObject.SetActive(true);
        }
        if (level == 3)
        {
            gameObject.transform.Find("Level2Star").gameObject.SetActive(false);
            gameObject.transform.Find("Level3Star").gameObject.SetActive(true);
        }
    }
}
