﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileHandler : MonoBehaviour
{
    public float projectileSpeed;

    private int projectileDamage;
    private float splashRadius = 2f;
    private float destroySelfDistance = 1f;
    private GameObject targetEnemy;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //find direction towards target
        Vector3 moveDirection = (targetEnemy.transform.position - gameObject.transform.position).normalized;

        //move towards target with projectileSpeed
        gameObject.transform.position += moveDirection * projectileSpeed * Time.deltaTime;

        //set rotation angle of projectile towards target
        gameObject.transform.eulerAngles = new Vector3(0, 0, getAngleFromVector(moveDirection));

        //destroy projectile if within destroySelfDistance of target
        if(Vector3.Distance(gameObject.transform.position, targetEnemy.transform.position) < destroySelfDistance)
        {
            //inflict splash damage if cannonball, single target damage otherwise
            if(gameObject.name == "Cannonball")
            {
                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

                foreach (GameObject enemy in enemies)
                {
                    //if enemy has died do not consider it
                    if (!enemy) continue;

                    //only damage enemies within radius of defense
                    if (Vector3.Distance(gameObject.transform.position, enemy.transform.position) <= splashRadius)
                    {
                        enemy.GetComponent<healthBar>().takeDamage(projectileDamage);
                    }
                }
            }
            else
            {
                targetEnemy.GetComponent<healthBar>().takeDamage(projectileDamage);
            }
            Destroy(gameObject);
        }

        //if targetEnemy has already died, destroy the projectile
        if (!targetEnemy) Destroy(gameObject);
    }

    public static void createProjectile(GameObject projectile, Vector3 spawnPosition, GameObject target, int damage)
    {
        GameObject proj = Instantiate(projectile, spawnPosition, projectile.transform.rotation);
        proj.name = proj.name.Replace("(Clone)", "");

        proj.GetComponent<projectileHandler>().targetEnemy = target;
        proj.GetComponent<projectileHandler>().projectileDamage = damage;
    }

    private static float getAngleFromVector(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;
    }


}
