﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour
{
    public static bool gamePaused = false;

    public GameObject pauseMenuUI;
    public GameObject buildMenuUI;
    public GameObject menuBgBlocker;
    public Animator fade;

    public void OnExitButtonPress()
    {
        //Debug.Log("Exit Button Clicked.");
        
        if(gamePaused)
        {
            resumeGame();
        } else
        {
            pauseGame();
        }
    }

    public void onResumeButtonPress()
    {
        //Debug.Log("Resume Button Clicked.");

        resumeGame();
    }

    public void onQuitButtonPress()
    {
        //Debug.Log("Quit Button Clicked.");

        quitGame();
    }

    private void resumeGame()
    {
        pauseMenuUI.SetActive(false);
        menuBgBlocker.SetActive(false);
        Time.timeScale = 1;
        gamePaused = false;

        structureFunctions.blockSelect = false;
        treeHarvest.blockHarvest = false;
    }

    private void pauseGame()
    {
        pauseMenuUI.SetActive(true);
        menuBgBlocker.SetActive(true);
        Time.timeScale = 0;
        gamePaused = true;

        if(buildMenuUI.activeSelf)
        {
            buildMenuUI.SetActive(false);
        }

        structureFunctions.blockSelect = true;
        treeHarvest.blockHarvest = true;
    }

    public void restartGame()
    {
        Time.timeScale = 1;
        StartCoroutine(loadSceneWithTransition("MainMenu"));
    }

    IEnumerator loadSceneWithTransition(string scene)
    {
        //Play animation
        fade.SetTrigger("Start");

        //Wait
        yield return new WaitForSeconds(1);

        //Load Scene & unload gamescene
        SceneManager.LoadScene(scene);
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
    }
    void quitGame()
    {
        Application.Quit();
    }
}
