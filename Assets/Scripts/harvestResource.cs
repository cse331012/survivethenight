﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class harvestResource : MonoBehaviour
{
    //Current harvester level
    private Harvester harvester;

    //variable to track runtime. used to track elapsed time in seconds
    private int runtime;

    // Start is called before the first frame update
    void Start()
    {
        updateHarvester();

        //get current runtime. used in update to increment resource each second.
        runtime = (int)Time.fixedTime;
    }

    // Update is called once per frame
    void Update()
    {
        //Time.fixedTime is current runtime in sec. update woodCount if second has elapsed
        if (waveCycle.dayTime && runtime != (int)Time.fixedTime)
        {
            //only collect resource if object placed
            if (!GetComponent<structureFunctions>().moveEnabled)
            {
                harvester.harvest();
            }
            runtime = (int)Time.fixedTime;
        }
    }

    public void updateHarvester()
    {
        harvester = (Harvester) gameObject.GetComponent<upgradeHandler>().structure;
    }
}
