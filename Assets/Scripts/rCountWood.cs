﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class rCountWood : MonoBehaviour
{
    public string rCountWoodStr = "<sprite=0>";
    public static int woodCount = 0;
    
    private TMP_Text rCountWoodUI;
    
    // Start is called before the first frame update
    void Start()
    {
        woodCount = 0;

        //Set reference to text
        rCountWoodUI = GetComponent<TMP_Text>();

        updateUIWoodCount();
    }

    // Update is called once per frame
    void Update()
    {
        updateUIWoodCount();
    }
    
    //Update wood count text in UI
    void updateUIWoodCount()
    {
        rCountWoodUI.text = rCountWoodStr + woodCount;
    }
}
