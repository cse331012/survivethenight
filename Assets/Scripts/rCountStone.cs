﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class rCountStone : MonoBehaviour
{
    public string rCountStoneStr = "<sprite=0>";
    public static int stoneCount;

    private TMP_Text rCountStoneUI;

    // Start is called before the first frame update
    void Start()
    {
        stoneCount = 0;

        //Set reference to text
        rCountStoneUI = GetComponent<TMP_Text>();

        updateUIWoodCount();
    }

    // Update is called once per frame
    void Update()
    {
        updateUIWoodCount();
    }

    //Update wood count text in UI
    void updateUIWoodCount()
    {
        rCountStoneUI.text = rCountStoneStr + stoneCount;
    }
}
