﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Pathfinding;

public class structureFunctions : MonoBehaviour
{
    //structure scriptable object
    private Structure structure;

    //determines whether structure is being moved.
    [HideInInspector]
    public bool moveEnabled = false;

    //variable to determine whether this is the initial placement of the object
    [HideInInspector]
    public bool initialPlacement = false;

    //determines if structure is selected;
    private bool selected = false;

    //blocks structure selection
    public static bool blockSelect = false;

    public GameObject boundary;

    private Vector2 mousePos;
    private Vector2 originalPos;

    // Start is called before the first frame update
    void Start()
    {
        structure = gameObject.GetComponent<upgradeHandler>().structure;
        //set name in structure select ui
        gameObject.transform.Find("StructureSelect").gameObject.transform.Find("Name").gameObject.GetComponent<TMP_Text>().text = gameObject.name;
    }

    // Update is called once per frame
    void Update()
    {
        //when moveEnabled, snap to grid positions
        if (moveEnabled)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (structure is Fence)
            {
                transform.position = new Vector2(Mathf.Round(mousePos.x) + 0.5f, Mathf.Round(mousePos.y) + 0.5f);
            }
            else
            {
                transform.position = new Vector2(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y));
            }
        }

        //deselect if detect click outside of UI
        if(selected)
        {
            if (outsideClick(gameObject.transform.Find("StructureSelect").gameObject)) deselectStructure();
        }
    }

    private void OnMouseDown()
    {
        if (selected)
        {
            deselectStructure();
        }
        else
        {
            if (moveEnabled)
            {
                placeStructure();
            }
            else if (!blockSelect)
            {
                selectStructure();
            }
        }
    }

    //enable structure select UI
    public void selectStructure()
    {
        treeHarvest.blockHarvest = true;
        selected = true;
        gameObject.transform.Find("StructureSelect").gameObject.SetActive(true);
    }

    //disable structure select UI
    public void deselectStructure()
    {
        treeHarvest.blockHarvest = false;
        selected = false;
        gameObject.transform.Find("StructureSelect").gameObject.SetActive(false);
    }

    private void placeStructure()
    {
        bool recalPathfinding = true;

        //if obstruction present or out of bounds, reset original position if not initial placing.
        //Delete object if initial placing and conditions not met.
        if (obstructionDetected() || !inBounds())
        {
            if (initialPlacement)
            {
                Debug.Log("Destroying " + gameObject.name);
                Destroy(gameObject);
                treeHarvest.blockHarvest = false;
                return;
            }
            transform.position = originalPos;
            recalPathfinding = false;
        }

        if (initialPlacement)
        {
            structure.takeBuildCost();
            gameObject.GetComponent<healthBar>().setMaxHealth(structure.health);
        }
        disableMove();

        //recalculate AI pathfinding graph
        if (recalPathfinding) AstarPath.active.Scan();
    }

    //detect outside click
    private bool outsideClick(GameObject structureSelectUI)
    {
        if (Input.GetMouseButton(0) && structureSelectUI.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                structureSelectUI.GetComponent<RectTransform>(),
                Input.mousePosition,
                Camera.main)) return true;
        return false;

    }

    //Check if obstruction present during placing
    private bool obstructionDetected()
    {
        int collisions = gameObject.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D(), new Collider2D[1]);
        if (collisions > 0) return true;
        return false;
    }

    private bool inBounds()
    {
        /*MeshCollider b = boundary.GetComponent<MeshCollider>();
        Debug.Log("bxmin - " + b.bounds.min.x + ",bxmax - " + b.bounds.max.x);
        Debug.Log("bymin - " + b.bounds.min.y + ",bymax - " + b.bounds.max.y);
        if (gameObject.transform.position.x < b.bounds.min.x ||
            gameObject.transform.position.y < b.bounds.min.y ||
            gameObject.transform.position.x > b.bounds.max.x ||
            gameObject.transform.position.y > b.bounds.max.y) return false;*/

        float posX = gameObject.transform.position.x;
        float posY = gameObject.transform.position.y;

        //check if in bounds
        if (posX < -13 || posY < -8  || posX > 13  || posY > 8)  return false;

        //check if over UI elements
        if ((posY == 8 && (posX <= -9 || posX >= 5)) || (posY == -8 && (posX <= -10 || posX >= 11))) return false;

        return true;
    }

    public void initialPlacing()
    {
        initialPlacement = true;
        enableMove();
    }

    public void enableMove()
    {
        moveEnabled = true;
        originalPos = transform.position;

        deselectStructure();

        treeHarvest.blockHarvest = true;
    }

    public void disableMove()
    {
        if (initialPlacement)
        {
            initialPlacement = false;
            if (structure is Defense) structure.setRadiusUI(gameObject);
        }
        moveEnabled = false;
        treeHarvest.blockHarvest = false;

        if(!waveCycle.dayTime)
        {
            gameObject.transform.Find("StructureSelect").transform.Find("MoveButton").gameObject.GetComponent<Button>().interactable = false; ;

        }
    }
}
