﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public Animator fade;

    public void playGame()
    {
        StartCoroutine(loadSceneWithTransition("GameScene"));
    }

    IEnumerator loadSceneWithTransition(string scene) {
        //Play animation
        fade.SetTrigger("Start");

        //Wait
        yield return new WaitForSeconds(1);

        //Load Scene
        SceneManager.LoadScene(scene);
    }

    public void exitGame()
    {
        Debug.Log("Quitting from Main Menu.");
        Application.Quit();
    }
}
