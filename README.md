# **Survive The Night**

Version 0.1
 
--------------
# Introduction
--------------

Survive The Night is a top-down tower defense game. Main Features:
- Objective: Fortify the player’s Home Base to survive as many waves of enemies as possible.
- Top-down wide view of play area.
- Player can gather resources to build Harvester and Defense structures.
- Harvester Structures will passively collect resources.
- Defense Structures will attack incoming enemies.
- Structures can be upgraded to power up player's base.
- Day/Night cycle system lets the player collect resources and build/upgrade Structures throughout the day to prepare to defend at night.
- Waves of enemies will attack at nighttime. Survive as many waves as possible!

*************************
# IMPORTANT NOTES
*************************
- Build steps are optional since there’s already an APK file built for you. Skip to Install if you don’t want to build from scratch.
- Please refer to Android SDK manual if you intend to run the game on a Virtual Device through Android Studio.
- If Android Studio is causing z-fighting glitches or crashes, ensure that Unity and Android Studio are using the same SDK, use a different virtual machine, or use an actual Android device.
- Please refer to your vendor’s manual if you intend to run the game on an emulator.
- Since the game has not been published. There might be warnings during the final step. Ignore to continue installing.

--------------
# Get the code
--------------

Go to your desired location and clone:

`git clone https://gitlab.com/cse331012/survivethenight.git`

or

Download directly.

------------------
# Build (Optional)
------------------

Required OS:
- Windows 10
- Linux with Wine supported
 
Required softwares:
- Lutris (Optional but recommended if Wine is not configured, Linux users only)
- Unity Engine 2019.4.19f1 or later with Android build support module
  (installing through Unity Hub is recommended) 

Steps:
1. Extract the source folder to a desired location.
2. Go to Projects on Unity Hub.
3. Select Add, browse and choose the source folder.
4. Select 2019.4.19f1 for Unity Version and Android for Target Platform.
5. Click the project name and wait for Unity to process files.
6. Once Unity opens, select Build Settings... from the File dropdown menu.
7. Select Build to build the project and save output at any destination.

---------
# Install
---------

1. Navigate to the Build folder where an APK file is ready.
2. For users with a physical Android device, connect the device to computer and copy the APK file into the device's storage. 
   For users with an Android emulator installed such as Nox or Bluestacks, copy the APK file into the shared storage with the emulator.
3. Press the game on the device to install it.

---------
# License
---------
Please read UNLICENSE for more details.
